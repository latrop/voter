from os import path
import shelve
import tkinter as Tk
from tkinter import ttk


class VotePanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.locked = False
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(column=2, row=0, columnspan=2)
        self.current_image = None

        self.revertable_actions = Tk.IntVar()
        self.revertable_actions.set(0)
        self.revertable_actions.trace("w", callback=self.update_revert_button_state)

        # Load voting elements from the config file
        element_idx = 0
        self.vote_variables = {}
        self.vote_variables_defaults = {}
        for line in open(self.window.config_params.vote_config):
            if line.startswith("#"):
                continue
            element_type = line.split()[0]
            element_name = line.split('"')[1]
            if element_type == "CB":
                self.vote_variables[element_name] = Tk.IntVar()
                self.vote_variables[element_name].set(0)
                Tk.Checkbutton(master=self.panel, text=element_name, font=("Times", 10),
                               variable=self.vote_variables[element_name]).grid(column=0, row=element_idx, padx=5,
                                                                                pady=5, sticky=Tk.W)
                element_idx += 1

            if element_type == "RB":
                lbl = Tk.Label(self.panel, text=element_name, font=("Times", 10))
                lbl.grid(column=0, row=element_idx, padx=10, sticky=Tk.W)
                element_idx += 1
                self.vote_variables[element_name] = Tk.StringVar()
                variants = line.split('"')[3::2]
                self.vote_variables[element_name].set(variants[0])
                for variant in variants:
                    print(variant)
                    Tk.Radiobutton(self.panel, text=variant, value=variant, font=("Times", 10),
                                   variable=self.vote_variables[element_name]).grid(column=0, row=element_idx,
                                                                                    sticky=Tk.W)
                    element_idx += 1

            if element_type == "EN":
                lbl = Tk.Label(self.panel, text=element_name, font=("Times", 10))
                lbl.grid(column=0, row=element_idx, padx=10, sticky=Tk.W)
                element_idx += 1
                self.vote_variables[element_name] = Tk.StringVar()
                e = Tk.Entry(self.panel, textvariable=self.vote_variables[element_name], font=("Times", 10), width=20)
                e.grid(column=0, row=element_idx, columnspan=2)
                element_idx += 1

            ttk.Separator(self.panel, orient="horizontal").grid(column=0, row=element_idx,
                                                                sticky=Tk.W+Tk.E, columnspan=2)
            element_idx += 1
            # Store default value
            self.vote_variables_defaults[element_name] = self.vote_variables[element_name].get()

        # Save button
        self.save_button = Tk.Button(self.panel, text="Save", command=self.save_votes,
                                     fg="green", font=("Times", 10))
        self.save_button.grid(column=0, row=element_idx)
        # Revert button
        self.revert_button = Tk.Button(self.panel, text="Revert", command=self.revert,
                                       fg="red", font=("Times", 10), state="disabled")
        self.revert_button.grid(column=1, row=element_idx)
        element_idx += 1

    def save_votes(self):
        self.vote_results = {}
        for element_name in self.vote_variables:
            self.vote_results[element_name] = self.vote_variables[element_name].get()
        db = shelve.open(path.join(self.window.results_dir, "vote_results.db"))
        for name in self.window.imag_panel.selected_images_names:
            key = path.split(name)[-1]
            db[key] = self.vote_results
        db.close()
        self.reset_votes()
        self.window.mark_as_done(self.window.imag_panel.selected_images_names)
        self.revertable_actions.set(self.revertable_actions.get() + 1)
        self.next()

    def reset_votes(self):
        # set all votes in the default positions
        for element_name in self.vote_variables:
            self.vote_variables[element_name].set(self.vote_variables_defaults[element_name])

    def next(self):
        # Remove selection
        self.window.imag_panel.unselect_all()
        # Show the next image and get everything ready to vote
        self.window.select_to_show()
        self.window.chunk_panel.show_chunk_info()
        self.window.imag_panel.show()

    def update_revert_button_state(self, *args):
        print(self.revertable_actions.get())
        if self.revertable_actions.get() > 0:
            self.revert_button.config(state="normal")
        else:
            self.revert_button.config(state="disabled")

    def revert(self):
        self.revertable_actions.set(self.revertable_actions.get()-1)
        # Get list of reverted images
        reverted_images = self.window.revert_last_images()
        # Remove reverted images from results database
        db = shelve.open(path.join(self.window.results_dir, "vote_results.db"))
        for name in reverted_images:
            del db[name]
        db.close()
        self.next()
