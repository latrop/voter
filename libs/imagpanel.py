#! /usr/bin/env python

import math
import tkinter as Tk
import pylab
import matplotlib.image as mpimg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class ImagPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(column=1, row=0)
        self.dpi = 96
        self.figsize_dots = int(0.9 * self.window.screen_height)
        self.figsize_inch = self.figsize_dots / self.dpi
        self.dots_per_image = self.figsize_dots / self.window.images_in_a_row
        self.mainGraph = pylab.Figure(figsize=(self.figsize_inch, self.figsize_inch), dpi=self.dpi)
        self.mainGraph.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.0, hspace=0.0)
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.panel)
        self.canvas.mpl_connect('button_press_event', self.onclick)
        self.figures = []
        self.tk_widget = self.canvas.get_tk_widget()
        self.tk_widget.grid(column=0, row=0)
        self.plot_instances = []
        self.selection_plot_instances = {}
        self.plotted_names = []
        self.selected_images = []
        self.selected_images_names = []

    def cleanup(self):
        # Delete existing stuff
        while len(self.figures) != 0:
            self.figures.pop().cla()
        self.mainGraph.clf()
        while len(self.plot_instances) != 0:
            self.plot_instances.pop().remove()

    def show(self):
        self.cleanup()

        # plot images
        fig_idx = 1
        if self.window.show_multiple:
            row_size = self.window.images_in_a_row
        else:
            row_size = 1
        self.plotted_names = []
        for i in range(row_size):
            for j in range(row_size):
                self.figures.append(self.mainGraph.add_subplot(row_size, row_size, fig_idx))
                self.figures[-1].axes.xaxis.set_ticks([])
                self.figures[-1].axes.yaxis.set_ticks([])
                if len(self.window.to_show) > fig_idx-1:
                    img = mpimg.imread(self.window.to_show[fig_idx-1])
                    self.plot_instances.append(self.figures[-1].imshow(img))
                    self.plotted_names.append(self.window.to_show[fig_idx-1])
                fig_idx += 1
        self.canvas.draw()

    def show_message(self, message):
        self.cleanup()
        self.figures.append(self.mainGraph.add_subplot(111))
        p = self.figures[0].annotate(s=message, xy=(0.5, 0.5), xycoords='axes fraction',
                                     ha='center', va='center', size=16, color="k")
        self.plot_instances.append(p)
        self.canvas.draw()

    def onclick(self, event):
        xclick = event.x
        yclick = self.figsize_dots - event.y
        i = int(math.modf(xclick / self.dots_per_image)[1])
        j = int(math.modf(yclick / self.dots_per_image)[1])
        idx = i + self.window.images_in_a_row * j
        if idx >= len(self.window.to_show):
            return
        image_name = self.plotted_names[idx]
        if (i, j) not in self.selected_images:
            # the image was not selected previously: add the selection
            self.selected_images.append((i, j))
            p = self.figures[idx].annotate(s="o", xy=(0.5, 0.5), xycoords='axes fraction',
                                           ha='center', va='center', size=36, color="r")
            self.selection_plot_instances[(i, j)] = p
            self.selected_images_names.append(image_name)

        else:
            # The image was selected prevoiosly: remove the selection
            self.selected_images.remove((i, j))
            self.selected_images_names.remove(image_name)
            self.selection_plot_instances.pop((i, j)).remove()

        self.canvas.draw()
        print(self.selected_images_names)

    def unselect_all(self):
        self.selected_images_names = []
        while len(self.selected_images) != 0:
            idx = self.selected_images.pop()
            self.selection_plot_instances.pop(idx).remove()
