#! /usr/bin/env python

import os
from os import path
import tkinter as Tk
import glob
import shutil
from .imagpanel import ImagPanel
from .votepanel import VotePanel
from .chunkpanel import ChunkPanel
from .config_parser import ConfigParams


class MainWindow(Tk.Frame):
    def __init__(self, *args, **kwargs):
        # program-wide definitions
        self.config_params = ConfigParams("config.dat")
        self.results_dir = "results_%s" % self.config_params.identifier
        self.log_name = path.join(self.results_dir, "log.dat")
        self.show_multiple = False
        self.images_in_a_row = 5  # Number of images to show in multi mode in each row
        self.images_to_process = []
        self.to_show = []

        # Make results directory
        if not os.path.exists(self.results_dir):
            os.makedirs(self.results_dir)

        # Create main window base frame
        self.root = Tk.Tk()
        self.screen_height = self.root.winfo_screenheight()
        # self.root.geometry("1000x800")
        # Left panel: contains chunk info
        self.chunk_panel = ChunkPanel(self)

        # Load image panel
        self.imag_panel = ImagPanel(self)

        # Load votes panel
        self.vote_panel = VotePanel(self)

        self.select_to_show()
        self.imag_panel.show()
        self.chunk_panel.show_chunk_info()
        # Start mainpool
        self.root.mainloop()

    def select_to_show(self):
        self.images_to_process = list(glob.glob(path.join("chunk", "*.jpg")))
        print(self.images_to_process)
        if len(self.images_to_process) == 0:
            return
        if self.show_multiple is False:
            self.to_show = [self.images_to_process[-1]]
            self.imag_panel.selected_images_names = [self.images_to_process[-1]]
        else:
            self.to_show = self.images_to_process[:self.images_in_a_row**2]
            self.imag_panel.selected_images_names = []

    def mark_as_done(self, names):
        for name in names:
            # Remove image from to-show list
            self.to_show.remove(name)
            # Move image file into results directory
            dst = path.join(self.results_dir, path.split(name)[-1])
            shutil.move(name, dst)
        # Save the step into the log file
        logfile = open(self.log_name, "a")
        log_string = ":".join(path.split(n)[-1] for n in names) + "\n"
        logfile.write(log_string)
        logfile.close()

    def revert_last_images(self):
        # Find last images that were saved
        logdata = open(self.log_name).readlines()
        last_line = logdata.pop().strip()
        last_images = last_line.split(":")
        # Move the last images from the results dir back to chunk
        for name in last_images:
            src = path.join(self.results_dir, name)
            dst = path.join("chunk", name)
            shutil.move(src, dst)
        # Save new log file
        logfile = open(self.log_name, "w")
        logfile.writelines(logdata)
        logfile.close()
        return last_images
