import glob
import os
from os import path
import tkinter as Tk
from tkinter import ttk
import time
import socket
import shutil
import paramiko

def http_proxy_tunnel_connect(proxy, target, timeout=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.connect(proxy)
    cmd_connect = "CONNECT %s:%d HTTP/1.1\r\n\r\n" % target
    sock.sendall(cmd_connect.encode())
    response = []
    sock.settimeout(2)
    while True:
        chunk = sock.recv(1024).decode()
        if not chunk:
            break
        response.append(chunk)
        if "\r\n\r\n" in chunk:
            break
    response = ''.join(response)
    return sock


class ChunkPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.LabelFrame(self.window.root, text="Chunk", font=("Times", 10))
        self.panel.grid(column=0, row=0)

        # Load chunk button
        self.load_chunk_button = Tk.Button(self.panel, text="Load chunk", font=("Times", 10),
                                           command=self.load_chunk, width=10)
        self.load_chunk_button.grid(column=0, row=0)
        ttk.Separator(self.panel, orient="horizontal").grid(column=0, row=1, sticky=Tk.W+Tk.E)
        # Show multiple button
        self.toggle_multi_button = Tk.Button(self.panel, text="Show multiple", width=10, font=("Times", 10),
                                             command=self.toggle_multiple)
        self.toggle_multi_button.grid(column=0, row=2)
        # Chunk info
        self.number_of_images = Tk.StringVar()
        self.number_of_images.set("Images in chunk: --")
        Tk.Label(self.panel, textvariable=self.number_of_images, width=19,
                 font=("Times", 10)).grid(column=0, row=3)
        self.votes_made = Tk.StringVar()
        self.votes_made.set("Votes made: --")
        Tk.Label(self.panel, textvariable=self.votes_made, width=19,
                 font=("Times", 10)).grid(column=0, row=4)

    def show_chunk_info(self):
        n = len(self.window.images_to_process)
        self.number_of_images.set("Images in chunk: %i" % n)
        if n == 0:
            self.load_chunk_button.config(state="normal")
        else:
            self.load_chunk_button.config(state="disabled")
        n = len(glob.glob(path.join(self.window.results_dir, "*.jpg")))
        self.votes_made.set("Votes made: %i" % n)

    def load_chunk(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if self.window.config_params.proxy_address is not None:
            # Make proxy connection if proxy parameters were provided
            proxy = (self.window.config_params.proxy_address, self.window.config_params.proxy_port)
            sock = http_proxy_tunnel_connect(proxy=proxy,
                                             target=(self.window.config_params.host, 22),
                                             timeout=50)
        else:
            sock = None
        ssh.connect(hostname=self.window.config_params.host,
                    username=self.window.config_params.login,
                    password=self.window.config_params.pwd,
                    sock=sock)
        while 1:
            cmd = './dispenser.py %s %s' % (self.window.config_params.identifier, self.window.images_in_a_row**2)
            stdin, stdout, stderr = ssh.exec_command(cmd)
            # there has to be only one line in the return
            line = stdout.read().decode("utf-8").strip()
            if "Empty" in line:
                self.window.imag_panel.show_message("All done. No new images in server.")
            if "Busy" in line:
                self.window.imag_panel.show_message("Server is busy. Trying again in 5 seconds...")
                time.sleep(5)
                continue
            break
        # Download chunk
        sftp = ssh.open_sftp()
        try:
            sftp.get(line, 'chunk.zip')
        except FileNotFoundError:
            return
        if not path.exists("chunk.zip"):
            # Something went wrong
            # TODO: Show error message
            return
        # Close connection
        sftp.close()
        ssh.close()
        # Unpack chunk
        shutil.unpack_archive('chunk.zip', extract_dir="chunk")
        os.remove('chunk.zip')

        # Disable revert button
        self.window.vote_panel.revertable_actions.set(0)

        # Update number of images in chunk
        self.toggle_multiple(new_state=True)
        self.show_chunk_info()

    def toggle_multiple(self, new_state=None):
        if new_state is None:
            new_state = not self.window.show_multiple
        self.window.show_multiple = new_state
        if new_state is True:
            self.toggle_multi_button.config(text="Show sinlge")
        else:
            self.toggle_multi_button.config(text="Show multiple")
        self.window.select_to_show()
        self.window.imag_panel.show()
