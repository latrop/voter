#! /usr/bin/env python


class ConfigParams(object):
    def __init__(self, config_file):
        self.proxy_address = None
        self.proxy_port = None
        for line in open(config_file):
            if line.startswith("#"):
                continue
            if line.startswith("vote_config"):
                self.vote_config = line.split()[1]
                continue
            if line.startswith("identifier"):
                self.identifier = line.split()[1]
                continue
            if line.startswith("proxy_address"):
                value = line.split()[1]
                if value != "none":
                    self.proxy_address = value
                continue
            if line.startswith("proxy_port"):
                value = line.split()[1]
                if value != "none":
                    self.proxy_port = value
                continue
            if line.startswith("login"):
                self.login = line.split()[1]
                continue
            if line.startswith("pwd"):
                self.pwd = line.split()[1]
                continue
            if line.startswith("host"):
                self.host = line.split()[1]
