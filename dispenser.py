#!/usr/bin/env python

import glob
import shelve
import shutil
import os
from os import path
import random
import argparse
import datetime


database_file = "images.db"
path_to_images = "flat"


class ImageFile(object):
    def __init__(self, path_to_file, times_to_show):
        self.path_to_file = path_to_file
        self.times_to_show = times_to_show
        self.list_of_viewers = []
        if times_to_show > 1:
            self.repeated = True
        else:
            self.repeated = False


def make_db(repeat_fraction=0.5, times_to_repeat=5):
    """
    database_file -- file to save database as a shelve object
    repeat_fraction -- fraction of files to show several times
    times_to_repeat -- how many times show repeated files
    """
    database = []
    for img_file in glob.glob(path.join(path_to_images, "*.jpg")):
        if random.random() < repeat_fraction:
            img = ImageFile(img_file, times_to_repeat)
        else:
            img = ImageFile(img_file, 1)
        database.append(img)
    db = shelve.open(database_file)
    db["images"] = database
    db.close()


def dispense_images(caller_name, number_of_images):
    """
    Give some images that this caller have never seen, and which counter is not equal to zero
    """
    # Open logfile
    t_string = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    logfile = open("image_dispenser.log", "a")
    logfile.write("%10s at %s\n" % (caller_name, t_string))
    # Load database
    db = shelve.open(database_file)
    database = db["images"]
    # Create temorary directory
    caller_dir = path.join("chunks", f"images_{caller_name}")
    if path.exists(caller_dir):
        shutil.rmtree(caller_dir)
    os.makedirs(caller_dir)
    imgs_counter = 0
    for idx in range(len(database)):
        if database[idx].times_to_show == 0:
            continue
        if caller_name in database[idx].list_of_viewers:
            # This caller have already seen this image
            continue
        shutil.copyfile(database[idx].path_to_file,
                        path.join(caller_dir, path.split(database[idx].path_to_file)[-1]))
        logfile.write("%s" % path.split(database[idx].path_to_file)[-1])
        if database[idx].repeated:
            logfile.write("  (repeat)\n")
        else:
            logfile.write("\n")
        # Add viewer so we wont show this image again
        database[idx].list_of_viewers.append(caller_name)
        # reduse times to show by one
        database[idx].times_to_show -= 1
        imgs_counter += 1
        if imgs_counter == number_of_images:
            break

    # save updated database
    db["images"] = database
    db.close()

    # Pack the images
    if imgs_counter > 0:
        shutil.make_archive(caller_dir, "zip", caller_dir)
        return path.join(os.getcwd(), f"{caller_dir}.zip")
    else:
        return "Empty"


def main(args):
    # Do now allow for more than one instance of the script to run simultaneously
    try:
        os.open("lock", os.O_CREAT | os.O_EXCL)
    except FileExistsError:
        print("Busy")
        exit()

    # Check of the database created
    if not path.exists(database_file):
        # If not, create one
        make_db()

    # Dispense images
    print(dispense_images(args.name, args.number))

    # unlock the script
    os.remove("lock")




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("name", help="Caller's name")
    parser.add_argument("number", help="Number of images to pack into the chunk", type=int, default=25)
    args = parser.parse_args()
    main(args)
